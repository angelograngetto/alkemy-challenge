import axios from 'axios'
import { API_URL } from './settings'

const editPost = (title, message, id) => {
  return axios.put(`${API_URL}/posts/${id}`, {
    title,
    body: message
  }).then((response) => {
    return response
  }).catch((error) => console.error(error))
}

export default editPost
