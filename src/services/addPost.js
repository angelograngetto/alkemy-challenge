import axios from 'axios'
import { API_URL } from './settings'

const addPost = (title, message) => {
  return axios.post(`${API_URL}/posts`, {
    title,
    body: message
  }).then((response) => {
    return response
  }).catch((error) => console.error(error))
}

export default addPost
