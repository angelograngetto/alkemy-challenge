import axios from 'axios'
import { API_URL } from './settings'

const deletePost = (id) => {
  return axios.delete(`${API_URL}/posts/${id}`)
    .then(response => {
      return response
    })
    .catch(error => console.error(error))
}

export default deletePost
