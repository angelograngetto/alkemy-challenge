import axios from 'axios'
import { API_URL } from './settings'

const getPosts = () => {
  return axios.get(`${API_URL}/posts`)
    .then(response => {
      return response.data
    })
}

export default getPosts
