import axios from 'axios'
import { API_URL } from './settings'

const getSinglePost = (id) => {
  return axios.get(`${API_URL}/posts/${id}`).then((response) => {
    return response.data
  }).catch((error) => console.error(error))
}

export default getSinglePost
