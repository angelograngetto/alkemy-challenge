import axios from 'axios'

const login = (email, password) => {
  return axios.post('http://challenge-react.alkemy.org/', {
    email,
    password
  }).then(response => {
    return response.data
  }).catch(error => console.error(error))
}

export default login
