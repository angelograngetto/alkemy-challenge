import { useEffect, useState } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import { Container } from 'react-bootstrap'
import getSinglePost from 'services/getSinglePost'

import Loader from 'components/Loader'
import PostForm from 'components/PostForm'

const EditPost = () => {
  const [post, setPost] = useState()
  const [loading, setLoading] = useState(true)
  const history = useHistory()
  const { id } = useParams()

  useEffect(() => {
    getSinglePost(id).then((response) => {
      if (response === undefined) {
        history.push('/404')
      } else {
        setPost(response)
        setLoading(false)
      }
    })
  }, [])

  if (loading) return <Loader/>

  return (
    <Container className="mt-5">
      <h2 className="text-center">Editar post</h2>
      <PostForm
        updatePost
        title={post.title}
        message={post.body}
        id={id}
      />
    </Container>
  )
}

export default EditPost
