import { Button, Container } from 'react-bootstrap'
import { Link } from 'react-router-dom'

const NotFound = () => {
  return (
    <Container className="d-flex flex-column align-items-center">
      <h2 className="text-center mt-5">404 error - No encontrado</h2>
      <Link to='/' className="mt-3"><Button>Ir al inicio</Button></Link>
    </Container>
  )
}

export default NotFound
