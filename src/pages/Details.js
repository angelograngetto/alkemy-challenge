import { useEffect, useState } from 'react'
import { useParams, useHistory } from 'react-router-dom'
import { Container } from 'react-bootstrap'
import getSinglePost from 'services/getSinglePost'
import Loader from 'components/Loader'
import { EditButton, DeleteButton } from 'components/ActionButtons'

const Details = () => {
  const history = useHistory()
  const { id } = useParams()
  const [post, setPost] = useState()
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    getSinglePost(id).then((response) => {
      if (response === undefined) {
        history.push('/404')
      } else {
        setPost(response)
        setLoading(false)
      }
    })
  }, [])

  if (loading) return <Loader/>

  return (
    <Container className="text-justify mt-5 border p-3 rounded shadow-sm">
      <h1>{post.title}</h1>
      <p>{post.body}</p>
      <hr/>
      <EditButton id={post.id}/>
      <DeleteButton id={post.id}/>
    </Container>
  )
}

export default Details
