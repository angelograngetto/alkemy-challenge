import PostForm from 'components/PostForm'
import { Container } from 'react-bootstrap'

const NewPost = () => {
  return (
    <Container className="mt-5">
      <h2 className="text-center">Agregar nuevo post</h2>
      <PostForm/>
    </Container>
  )
}

export default NewPost
