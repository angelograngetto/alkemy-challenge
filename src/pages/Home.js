import { useState, useEffect } from 'react'
import { Container } from 'react-bootstrap'
import getPosts from 'services/getPosts'
import PostCard from 'components/PostCard'
import Loader from 'components/Loader'

const Home = () => {
  const [posts, setPosts] = useState()
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    setLoading(true)
    getPosts().then(res => {
      setPosts(res)
      setLoading(false)
    })
  }, [])

  if (loading) return <Loader/>

  return (
      <Container className='mt-5'>
        <h2 className='text-center'>Mostrando todos los posts</h2>
        <div className="row">
          {posts.map(post => (
            <div className='col-md-4 mb-2 text-center' key={post.id}>
              <PostCard title={post.title} id={post.id}/>
            </div>
          ))}
        </div>
      </Container>
  )
}

export default Home
