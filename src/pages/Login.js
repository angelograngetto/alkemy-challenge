import { useState } from 'react'
import { Formik, Field } from 'formik'
import { useSelector, useDispatch } from 'react-redux'
import { signIn } from 'reducers/userReducer'
import { Container, Spinner, Alert } from 'react-bootstrap'
import { useHistory } from 'react-router-dom'
import login from 'services/login'

const Login = () => {
  const [loading, setLoading] = useState(false)
  const history = useHistory()
  const user = useSelector(state => state)
  const dispatch = useDispatch()

  user && history.push('/')

  return (
    <>
      <Container className="w-100 mt-5 d-flex flex-column align-items-center">
        <h1 className=''>Ingresar</h1>
        <Formik
                initialValues={{ email: '', password: '' }}
                validate={ values => {
                  const errors = {}
                  if (!values.email) errors.email = 'Email requerido'
                  if (!values.password) errors.password = 'Contraseña requerida'
                  return errors
                }}
                onSubmit={(values, { setFieldError }) => {
                  setLoading(true)
                  login(values.email, values.password).then((response) => {
                    dispatch(signIn(response.token))
                    history.push('/')
                  }).catch(() => {
                    setFieldError('account', 'Email o contraseña incorrectos.')
                    setLoading(false)
                  })
                }}
            >
                {({ errors, handleChange, handleSubmit }) => <form onSubmit={handleSubmit} className="w-75 border p-3 rounded shadow-sm">
                        <label htmlFor="email">Tu email</label>
                        <Field
                          onChange={handleChange}
                          name="email"
                          type="email"
                          placeholder="email@dominio.com"
                          className="form-control d-block m-auto mb-2"
                        />
                        {errors.email && <Alert variant="danger">{errors.email}</Alert>}
                        <label htmlFor="email">Tu contraseña</label>
                        <Field
                          onChange={handleChange}
                          name="password"
                          type="password"
                          placeholder="contraseña"
                          className="form-control d-block m-auto mb-2"
                        />
                        {errors.password && <Alert variant="danger">{errors.password}</Alert>}
                        {errors.account && <Alert variant="danger">{errors.account}</Alert>}
                        <button type="submit" className="btn btn-primary mt-2" disabled={loading}>
                          {loading
                            ? <Spinner
                              as="span"
                              animation="border"
                              size="sm"
                              role="status"
                              aria-hidden="true"
                            />
                            : 'Entrar'}
                          </button>
                    </form>}
            </Formik>
      </Container>
    </>
  )
}

export default Login
