import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import Menu from './components/Menu'
import PrivateRoute from 'components/PrivateRoute'

import Home from 'pages/Home'
import Login from 'pages/Login'
import Details from 'pages/Details'
import NewPost from 'pages/NewPost'
import EditPost from 'pages/EditPost'
import NotFound from 'pages/NotFound'

const App = () => {
  return (
    <div>
      <Router>
          <Menu/>
          <Switch>
                <Route path='/login' component={Login}/>
                <PrivateRoute path='/nuevo' component={NewPost}/>
                <PrivateRoute path='/editar/:id' component={EditPost}/>
                <PrivateRoute path='/detalles/:id' component={Details}/>
                <PrivateRoute exact path='/' component={Home}/>
                <Route path='*' component={NotFound}/>
          </Switch>
      </Router>
    </div>
  )
}

export default App
