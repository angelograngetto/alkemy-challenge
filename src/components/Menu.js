import { Navbar, Nav, Container } from 'react-bootstrap'
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import styles from './Menu.module.css'
import { signOut } from 'reducers/userReducer'

const Menu = () => {
  const user = useSelector(state => state)
  const dispatch = useDispatch()
  return (
      <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark" sticky="top">
        <Container>
          <Navbar.Brand><Link to='/' className={styles.link}>Alkemy Challenge</Link></Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          { user && <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="me-auto">
              <Navbar.Text><Link to='/nuevo' className={styles.link}>Agregar post</Link></Navbar.Text>
              <Navbar.Text><a href='https://bitbucket.org/angelograngetto/alkemy-challenge/src/master/' className={styles.link} target='_blank' rel="noreferrer">Repositorio</a></Navbar.Text>
            </Nav>
            <Nav>
              <Navbar.Text className={styles.link} onClick={() => dispatch(signOut())}>Salir</Navbar.Text>
            </Nav>
          </Navbar.Collapse>
          }
        </Container>
      </Navbar>
  )
}

export default Menu
