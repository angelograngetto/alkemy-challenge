import { Spinner } from 'react-bootstrap'

const Loader = () => {
  return (
    <div className="d-flex justify-content-center pt-5">
      <Spinner className="d-flex justify-content-center" animation="grow" />
    </div>
  )
}

export default Loader
