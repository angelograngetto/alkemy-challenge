import styles from './PostCard.module.css'
import { Link } from 'react-router-dom'
import { EditButton, DeleteButton } from 'components/ActionButtons'

const PostCard = ({ title, id }) => {
  return (
    <div className="border rounded shadow-sm p-2 m-2 h-100 mb-2 ">
      <h4>
        <Link to={`detalles/${id}`} className={styles.title}>
          {title.charAt(0).toUpperCase() + title.slice(1)}
        </Link>
      </h4>
      <hr/>
      <EditButton id={id}/>
      <DeleteButton id={id}/>
    </div>
  )
}

export default PostCard
