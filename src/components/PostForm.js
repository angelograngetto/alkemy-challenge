import { useState } from 'react'
import { useHistory } from 'react-router-dom'
import { Spinner, Alert } from 'react-bootstrap'
import { Field, Formik } from 'formik'
import addPost from 'services/addPost'
import editPost from 'services/editPost'

const PostForm = ({ title = '', message = '', updatePost = false, id = 1 }) => {
  const history = useHistory()
  const [loading, setLoading] = useState(false)
  return (
    <Formik
      initialValues={{ title, message }}
      validate={ values => {
        const errors = {}
        if (!values.title) errors.title = 'Título requerido'
        if (values.title.length < 3) errors.title = 'El título debe contener más de 3 caracteres.'
        if (!values.message) errors.message = 'Mensaje requerido'
        if (values.message.length < 3) errors.message = 'El post debe contener más de 3 caracteres.'
        return errors
      }}
      onSubmit={(values, { setFieldError }) => {
        if (updatePost) {
          setLoading(true)
          editPost(values.title, values.message, id).then((response) => {
            setLoading(false)
            if (response.status === 200) {
              history.push('/')
            } else {
              setFieldError('general', 'Hubo un error al guardar.')
            }
          })
        } else {
          setLoading(true)
          addPost(values.title, values.message).then((response) => {
            setLoading(false)
            if (response.status === 201) {
              history.push('/')
            } else {
              setFieldError('general', 'Hubo un error al guardar.')
            }
          })
        }
      }}
   >
    {({ errors, handleChange, handleSubmit }) => <form onSubmit={handleSubmit} className="border p-3 rounded shadow-sm">
            <label htmlFor="title">Título del post:</label>
            <Field
              onChange={handleChange}
              name='title'
              type="text"
              className="form-control mb-2 mt-2"
              placeholder="Escribe aquí el título"
            />
            {errors.title && <Alert variant="danger">{errors.title}</Alert>}
            <label htmlFor="message">Mensaje del post:</label>
            <Field
              as="textarea"
              onChange={handleChange}
              name='message'
              className="form-control mb-2 mt-2"
              placeholder="Escribe aquí el post"
            />
            {errors.message && <Alert variant="danger">{errors.message}</Alert>}
            {errors.general && <Alert variant="danger">{errors.general}</Alert>}
            <button type="submit" className="btn btn-primary mt-2" disabled={loading}>
            {loading
              ? <Spinner
                as="span"
                animation="border"
                size="sm"
                role="status"
                aria-hidden="true"
              />
              : 'Guardar'}
            </button>
        </form>}
    </Formik>
  )
}

export default PostForm
