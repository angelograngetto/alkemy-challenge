import { useHistory } from 'react-router-dom'
import { Button } from 'react-bootstrap'
import { MdEdit, MdDeleteForever } from 'react-icons/md'
import deletePost from 'services/deletePost'

export const EditButton = ({ id }) => {
  const history = useHistory()

  const handleEdit = (id) => {
    history.push(`/editar/${id}`)
  }

  return (
        <Button
          size="sm"
          variant="warning"
          className="me-1 rounded-circle"
          title="Editar"
          onClick={() => handleEdit(id)}
          >
            <MdEdit color='white'/>
        </Button>
  )
}

export const DeleteButton = ({ id }) => {
  const history = useHistory()

  const handleDelete = (id) => {
    const confirmDelete = confirm('¿Seguro quieres eliminar este post?')
    if (confirmDelete) {
      deletePost(id).then(response => {
        if (response.status === 200) {
          alert('Post eliminado.')
          history.push('/')
        } else {
          alert('Hubo un error al eliminar el post.')
        }
      })
    }
  }

  return (
          <Button
            size="sm"
            variant="danger"
            className="me-1 rounded-circle"
            title="Eliminar"
            onClick={() => handleDelete(id)}
          >
            <MdDeleteForever color='white'/>
          </Button>
  )
}
