const TYPES = {
  USER_SIGN_IN: '@user/signin',
  USER_SIGN_OUT: '@user/signout'
}

export const userReducer = (state = localStorage.getItem('tkn'), action) => {
  switch (action.type) {
    case TYPES.USER_SIGN_IN: {
      localStorage.setItem('tkn', action.payload)
      return action.payload
    }
    case TYPES.USER_SIGN_OUT: {
      localStorage.removeItem('tkn')
      return null
    }
    default:
      return state
  }
}

export const signIn = (token) => {
  return {
    type: TYPES.USER_SIGN_IN,
    payload: token
  }
}

export const signOut = () => {
  return {
    type: TYPES.USER_SIGN_OUT
  }
}
